#####
# cluster loops based on the overlapping of both sides
#####


# input
file_bedpe=$1 # bedpe file
nIteration=$2 # # of iteration to perform
gap=$3 # gaps in bp to merge


# make a copy of the bedpe file to work on
round_bedpe="$(mktemp XXXXXX)"
cp $file_bedpe $round_bedpe


# start running the pre- iterative clustering algorithm
COUNTER=1
while [ $COUNTER -le $nIteration ]; do

	## report the current number of iteration
	echo Iteration round: $COUNTER

	## sperate the bedpe into two files
	awk 'NR%2==1' $round_bedpe > $round_bedpe.odd
	awk 'NR%2==0' $round_bedpe > $round_bedpe.even

	## find the both anchor overlapped loops
	pairToPair -is -type both -slop $gap -a $round_bedpe.odd -b $round_bedpe.even |awk '$2"."$3"."$5"."$6"."$7!=$9"."$10"."$12"."$13"."$14' > $round_bedpe.both

	## stop if the algorithm converged
	if [ ! -s $round_bedpe.both ]; then
		echo Pre-clustering converged!!!
		break
	fi

	## get the loops with no overlaps on both anchors
	awk 'NR==FNR {a[$0]} NR>FNR && !($0 in a) {print $0}' <(cat <(cut -f1-7 $round_bedpe.both) <(cut -f8-14 $round_bedpe.both)) $round_bedpe > $round_bedpe.notboth

	## group the cluster of the matches together
	bedtools groupby -i $round_bedpe.both -g 1,2,3,4,5,6,7 -c 8,9,10,11,12,13,14 -o first,min,max,first,min,max,distinct > $round_bedpe.both.clusters

	## merge overlapped loops
	awk '{stt1=($2<$9?$2:$9); end1=($3>$10?$3:$10); stt2=($5<$12?$5:$12); end2=($6>$13?$6:$13); print $1"\t"stt1"\t"end1"\t"$4"\t"stt2"\t"end2"\t"$7","$14}' $round_bedpe.both.clusters > $round_bedpe.both.merge

	## get a unique sample list the loops are detected
	bedtools expand -i  $round_bedpe.both.merge -c 7 |sed 's/;/\t/g' |sort -u >  $round_bedpe.both.merge.expanded
	bedtools groupby -i  $round_bedpe.both.merge.expanded -g 1,2,3,4,5,6,7 -c 8 -o max >  $round_bedpe.both.merge.grouped.1
	awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7";"$8}' $round_bedpe.both.merge.grouped.1 |bedtools groupby -i - -g 1,2,3,4,5,6 -c 7 -o distinct >  $round_bedpe.both.merge.grouped

	## combine all loops
	cat  $round_bedpe.both.merge.grouped  $round_bedpe.notboth > $round_bedpe

	## report the progress
	nLeft=`cat $round_bedpe.both.merge.grouped |wc -l`
	echo "# of clusters to go for the next iteration: $nLeft"

	## return if hit the max iteration
	if [ $COUNTER -eq $nIteration ]; then
		echo "$nIteration iterations completed...not fully converged yet!!!"
	fi

	## increasing the counter
	let COUNTER=COUNTER+1

done

# start running the complete- iterative clustering algorithm
COUNTER=1
while [ $COUNTER -le $nIteration ]; do

	## report the current number of iteration
	echo Iteration round: $COUNTER

	## find the both anchor overlapped loops
	pairToPair -is -type both -slop $gap -a $round_bedpe -b $round_bedpe |awk '$2"."$3"."$5"."$6"."$7!=$9"."$10"."$12"."$13"."$14' > $round_bedpe.both

	## stop if the algorithm converged
	if [ ! -s $round_bedpe.both ]; then
		echo Clustering converged!!!
		break
	fi

	## get the loops with no overlaps on both anchors
	awk 'NR==FNR {a[$0]} NR>FNR && !($0 in a) {print $0}' <(cut -f1-7 $round_bedpe.both) $round_bedpe > $round_bedpe.notboth

	## group the cluster of the matches together
	bedtools groupby -i $round_bedpe.both -g 1,2,3,4,5,6,7 -c 8,9,10,11,12,13,14 -o first,min,max,first,min,max,distinct > $round_bedpe.both.clusters

	## merge overlapped loops
	awk '{stt1=($2<$9?$2:$9); end1=($3>$10?$3:$10); stt2=($5<$12?$5:$12); end2=($6>$13?$6:$13); print $1"\t"stt1"\t"end1"\t"$4"\t"stt2"\t"end2"\t"$7","$14}' $round_bedpe.both.clusters > $round_bedpe.both.merge

	## get a unique sample list the loops are detected
	bedtools expand -i  $round_bedpe.both.merge -c 7 |sed 's/;/\t/g' |sort -u >  $round_bedpe.both.merge.expanded
	bedtools groupby -i  $round_bedpe.both.merge.expanded -g 1,2,3,4,5,6,7 -c 8 -o max >  $round_bedpe.both.merge.grouped.1
	awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7";"$8}' $round_bedpe.both.merge.grouped.1 |bedtools groupby -i - -g 1,2,3,4,5,6 -c 7 -o distinct >  $round_bedpe.both.merge.grouped

	## combine all loops
	cat  $round_bedpe.both.merge.grouped  $round_bedpe.notboth > $round_bedpe

	## report the progress
	nLeft=`cat $round_bedpe.both.merge.grouped |wc -l`
	echo "# of clusters to go for the next iteration: $nLeft"

	## return if hit the max iteration
	if [ $COUNTER -eq $nIteration ]; then
		echo "$nIteration iterations completed...not fully converged yet!!!"
	fi

	## increasing the counter
	let COUNTER=COUNTER+1

done

# output
sort -k1,1 -k2,2n -k3,3n -k5,5n -k6,6n $round_bedpe > $file_bedpe.cluster

# clean tmp files
rm $round_bedpe*

# return the output file name
echo "CLustered loops are in file: $file_bedpe.cluster"
