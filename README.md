# README for loop_cluster package #
--------
***loop_cluster*** is designed to	cluster and merge redundant Hi-C loops.
We designed a two-step iterative clustering algorithm to cluster and merge paired-end loops within a certain genomic range cutoff; here we used a 1 kb region of boundary overlaps. In the pre-clustering step, we ranked all loops by their chromosome position and subsequently divided them into two groups based on whether they had even or odd ranks. We then used the pairToPair command in bedtools (v2.25.0) to investigate the overlaps of boundaries between any paired loops from the two sets. The loops in one set that overlapped with any loops in the other set were merged to form new loops with union boundary regions. The loops in one set having no overlaps with any loops in the other set were retained. The merged and retained loops were used as the input for the next iteration. We iteratively applied this process N=50 times to obtain pre-clustered loops. In the complete-clustering step, we used the same strategy as in the previous step, except for searching for the overlaps between the pre-clustered loops and other loops in the same set, instead of dividing them into two loop sets. Self-pairs were excluded from the analysis. In this step, the iterations were continued until the algorithm converges and no paired-end loops can be merged anymore. This two-step procedure was able to cluster and merge a large number of redundant loops in any given genomic range cutoff in a short time period.  

### How to download loop_cluster? ###
--------
Download the package using git clone or using the "download" link. (less than 1 minutes)

     git clone https://bitbucket.org/mthjwu/loop_cluster

### Software dependencies ###
--------
The package has been tested in Linux.

* bedtools v2.25.0

### How to run loop_cluster ###
--------
* run test data (less than 1 minutes)

        bash loop_cluster.sh test.bedpe 50 1000

* Parameters: <bedpe> <# of iterations to perform> <gaps (in bp) to merge>

### Who do I talk to? ###
--------
* Hua-Jun Wu (mthjwu@gmail.com)
